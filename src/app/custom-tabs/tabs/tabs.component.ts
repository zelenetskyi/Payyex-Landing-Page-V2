import {
  Component, ContentChildren, QueryList, AfterContentInit, Output, EventEmitter, Input,
  OnInit
} from '@angular/core';
import { TabComponent } from '../tab/tab.component';

@Component({
  selector: 'custom-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss'],
})
export class TabsComponent implements AfterContentInit {

  @Output()
  opened = new EventEmitter<number>();
  @Input()
  activeTabs;
  @Input()
  setFirst;

  @ContentChildren(TabComponent) tabs: QueryList<TabComponent>;

  // contentChildren are set
  ngAfterContentInit() {
    if ( this.activeTabs ) {
      // get all active tabs
      let tabsWithActive = this.tabs.filter((tab)=>tab.active);

      // if there is no active tab set, activate the first
      if ( tabsWithActive.length === 0 ) {
        this.selectTab(this.tabs.first, 0);
      }
    }
  }

  selectTab(tab: TabComponent, index) {
    // deactivate all tabs
    this.tabs.toArray().forEach(tab => tab.active = false);
    // console.log('tab', tab);

    // activate the tab the user has clicked on.
    setTimeout(() => tab.active = true);

    this.opened.emit(index);
  }

}