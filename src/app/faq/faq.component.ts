import { Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, ViewChild } from '@angular/core';
import { faqList } from '../faq-info';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss'],
})
export class FaqComponent implements OnInit {

  searchText: string = '';
  faqShowId: number = 0;
  notFound = false;
  linkBack: boolean = false;
  faqOpened: boolean = false;
  activeCheck: boolean = false;
  faqList = faqList;

  @HostListener('input') onInput() {
    let items = this.faqList;
    let itemsLength: number = items.length;
    let changedItemsLength: number = items.length;
    if ( !items ) return [];
    this.searchText = this.searchText.toLowerCase();
    items =  items.filter(it => {
      if ( it.question.toLowerCase().indexOf(this.searchText) > -1 ) {
        // this.notFound = false;
      } else {
        // this.notFound = true;
      }
      // console.log(this.notFound);
      return it.question.toLowerCase().includes(this.searchText)
    });
    changedItemsLength = items.length;
    console.log(itemsLength, 'items length');
    console.log(changedItemsLength, 'changed items length');
    if ( itemsLength === changedItemsLength ) {
      this.linkBack = false;
      this.notFound = false;
    } else if ( changedItemsLength === 0 ) {
      this.linkBack = true;
      this.notFound = true;
    } else {
      this.linkBack = true;
      this.notFound = false;
      return items;
    }
  }
  constructor() {
  }

  openFaq(id) {
    this.faqOpened = true;
    this.faqShowId = this.faqList[id].id;
    window.scrollTo(0, 0);
  }

  checkIfActivate(id) {
    this.openFaq(id);
  }
  // checkIfRemoveActive(value) {
  //   this.closeFaq(value);
  // }

  closeFaq() {
    this.faqOpened = false;
    this.searchText = '';
    window.scrollTo(0, 0);
  }

  backToMain() {
    this.linkBack = false;
    this.notFound = false;
    this.faqOpened = false;
    this.searchText = '';
  }

  ngOnInit() {
  }

}
