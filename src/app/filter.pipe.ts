import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter', pure: false
})

@Injectable()
export class FilterPipe implements PipeTransform {

  transform(items: any[], searchText: string): any[] {
    if ( !items ) return [];
    if ( !searchText ) return items;

    searchText = searchText.toLowerCase();
    return items.filter(it => {
      if ( it.question.toLowerCase().indexOf(searchText) > -1 ) {
        // this.notFound = false;
      } else {
        // this.notFound = true;
      }
      // console.log(this.notFound);
      return it.question.toLowerCase().includes(searchText)
    });
  }

}
