import { AfterContentInit, Component } from '@angular/core';
import { ApiDocsService } from '../apidocs.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-apidocs',
  templateUrl: './apidocs.component.html',
  styleUrls: ['./apidocs.component.scss'],
  providers: [ApiDocsService]
})
export class ApidocsComponent implements AfterContentInit {

  listOfPages;
  commonApiApies: Array<any> = [];
  commonApiApiesPaths: Array<any> = [];
  commonApiApiesParams: Array<string> = [];
  commonApiTypes: Array<any> = [];
  commonApiModels: Array<any> = [];
  commonOpenedInfo: Array<boolean> = [];
  commonRequestUrl: Array<any> = [];
  commonResponseBody: Array<any> = [];
  commonResponseCode: Array<any> = [];
  commonResponseHeaders: Array<any> = [];
  commonRequestResult: Array<any> = [];
  userGroupsApiApies: Array<any> = [];
  userGroupsApiApiesPaths: Array<any> = [];
  userGroupsApiApiesParams: Array<string> = [];
  userGroupsApiTypes: Array<any> = [];
  userGroupsApiModels: Array<any> = [];
  userGroupsOpenedInfo: Array<boolean> = [];
  userGroupsRequestUrl: Array<any> = [];
  userGroupsResponseBody: Array<any> = [];
  userGroupsResponseCode: Array<any> = [];
  userGroupsResponseHeaders: Array<any> = [];
  userGroupsRequestResult: Array<any> = [];
  exchangeApiApies: Array<any> = [];
  exchangeApiApiesPaths: Array<any> = [];
  exchangeApiApiesParams: Array<string> = [];
  exchangeApiTypes: Array<any> = [];
  exchangeApiModels: Array<any> = [];
  exchangeOpenedInfo: Array<boolean> = [];
  exchangeRequestUrl: Array<any> = [];
  exchangeResponseBody: Array<any> = [];
  exchangeResponseCode: Array<any> = [];
  exchangeResponseHeaders: Array<any> = [];
  exchangeRequestResult: Array<any> = [];
  walletApiApies: Array<any> = [];
  walletApiApiesPaths: Array<any> = [];
  walletApiApiesParams: Array<string> = [];
  walletApiTypes: Array<any> = [];
  walletApiModels: Array<any> = [];
  walletOpenedInfo: Array<boolean> = [];
  walletRequestUrl: Array<any> = [];
  walletResponseBody: Array<any> = [];
  walletResponseCode: Array<any> = [];
  walletResponseHeaders: Array<any> = [];
  walletRequestResult: Array<any> = [];
  posApiApies: Array<any> = [];
  posApiApiesPaths: Array<any> = [];
  posApiApiesParams: Array<string> = [];
  posApiTypes: Array<any> = [];
  posApiModels: Array<any> = [];
  posOpenedInfo: Array<boolean> = [];
  posRequestUrl: Array<any> = [];
  posResponseBody: Array<any> = [];
  posResponseCode: Array<any> = [];
  posResponseHeaders: Array<any> = [];
  posRequestResult: Array<any> = [];

  constructor(
    private apiDocsService: ApiDocsService
  ) {}

  getApiDocsList() {
    this.apiDocsService.getDocsApi('list')
      .subscribe(result => {
        this.listOfPages = result.apis;
      });
  }
  getApiDocsPage() {
    let commonTypesArray: Array<string> = [];
    let userGroupsTypesArray: Array<string> = [];
    let exchangeTypesArray: Array<string> = [];
    let walletTypesArray: Array<string> = [];
    let posTypesArray: Array<string> = [];

    this.apiDocsService.getDocsApi('common')
      .subscribe(result => {
        result.apis.forEach(element => {
          // get type of method ( public or private )
          commonTypesArray.push(element.path);
          this.commonApiApiesPaths.push(element.path);
          element.operations.forEach(element => {
            this.commonApiApies.push(element);
          });
        });
        for (let key in result.models) {
          if (result.models.hasOwnProperty(key)) {
            this.commonApiModels[key] = result.models[key].properties;
          }
        }
        let getReferralModelsFromElement = function( element, instance ) {
          let referral_models = [];
          if( typeof element.type !== 'undefined' ) {
            let tmp_properties = instance.commonApiModels[element.type];
            referral_models.push( element.type );
            const getTypesFromModels = function( real_properties ) {
              for( let key in real_properties ) {
                if(real_properties.hasOwnProperty(key) ) {
                  if( typeof real_properties[key].$ref !== 'undefined' ) {
                    referral_models.push(real_properties[key].$ref);
                  } else if( typeof real_properties[key].items !== 'undefined' && typeof real_properties[key].items.$ref !== 'undefined' ) {
                    referral_models.push(real_properties[key].items.$ref);
                  }
                }
              }
              return referral_models;
            };
            referral_models = getTypesFromModels( tmp_properties );
            const filter_unique = function( value, index, self ) {
              return self.indexOf(value) === index;
            };
            referral_models = referral_models.filter( filter_unique );
            for( let i = 0; i < referral_models.length; i++ ) {
              referral_models = getTypesFromModels( instance.commonApiModels[ referral_models[i] ] );
            }
            referral_models = referral_models.filter( filter_unique );
            return referral_models;
          }

        };
        this.commonApiApies.forEach(element => {
            element.referral_models = getReferralModelsFromElement( element, this );
            this.commonApiApiesParams.push( element.parameters );
        });
        commonTypesArray.forEach(element => {
          if ( element.indexOf('private') >= 0 ) {
            this.commonApiTypes.push("Private");
          } else {
            this.commonApiTypes.push("Public");
          }
        });
      });
    this.apiDocsService.getDocsApi('user-groups')
      .subscribe(result => {
        result.apis.forEach(element => {
          // get type of method ( public or private )
          userGroupsTypesArray.push(element.path);
          this.userGroupsApiApiesPaths.push(element.path);
          element.operations.forEach(element => {
            this.userGroupsApiApies.push(element);
          });
        });
        let getReferralModelsFromElement = function( element, instance ) {
          let referral_models = [];
          if( typeof element.type !== 'undefined' ) {
            let tmp_properties = instance.userGroupsApiModels[element.type];
            referral_models.push( element.type );
            const getTypesFromModels = function( real_properties ) {
              for( let key in real_properties ) {
                if(real_properties.hasOwnProperty(key) ) {
                  if( typeof real_properties[key].$ref !== 'undefined' ) {
                    referral_models.push(real_properties[key].$ref);
                  } else if( typeof real_properties[key].items !== 'undefined' && typeof real_properties[key].items.$ref !== 'undefined' ) {
                    referral_models.push(real_properties[key].items.$ref);
                  }
                }
              }
              return referral_models;
            };
            referral_models = getTypesFromModels( tmp_properties );
            const filter_unique = function( value, index, self ) {
              return self.indexOf(value) === index;
            };
            referral_models = referral_models.filter( filter_unique );
            for( let i = 0; i < referral_models.length; i++ ) {
              referral_models = getTypesFromModels( instance.userGroupsApiModels[ referral_models[i] ] );
            }
            referral_models = referral_models.filter( filter_unique );
            return referral_models;
          }
        };
        this.userGroupsApiApies.forEach(element => {
          element.referral_models = getReferralModelsFromElement( element, this );
          this.userGroupsApiApiesParams.push( element.parameters );
        });
        for (let key in result.models) {
          if (result.models.hasOwnProperty(key)) {
            this.userGroupsApiModels[key] = result.models[key].properties;
          }
        }
        userGroupsTypesArray.forEach(element => {
          if ( element.indexOf('private') >= 0 ) {
            this.userGroupsApiTypes.push("Private");
          } else {
            this.userGroupsApiTypes.push("Public");
          }
        });
      });
    this.apiDocsService.getDocsApi('exchange')
      .subscribe(result => {
        result.apis.forEach(element => {
          // get type of method ( public or private )
          exchangeTypesArray.push(element.path);
          this.exchangeApiApiesPaths.push(element.path);
          element.operations.forEach(element => {
            this.exchangeApiApies.push(element);
          });
        });
        for (let key in result.models) {
          if (result.models.hasOwnProperty(key)) {
            this.exchangeApiModels[key] = result.models[key].properties;
          }
        }
        let getReferralModelsFromElement = function( element, instance ) {
          let referral_models = [];
          if( typeof element.type !== 'undefined' ) {
            let tmp_properties = instance.exchangeApiModels[element.type];
            referral_models.push( element.type );
            const getTypesFromModels = function( real_properties ) {
              for( let key in real_properties ) {
                if(real_properties.hasOwnProperty(key) ) {
                  if( typeof real_properties[key].$ref !== 'undefined' ) {
                    referral_models.push(real_properties[key].$ref);
                  } else if( typeof real_properties[key].items !== 'undefined' && typeof real_properties[key].items.$ref !== 'undefined' ) {
                    referral_models.push(real_properties[key].items.$ref);
                  }
                }
              }
              return referral_models;
            };
            referral_models = getTypesFromModels( tmp_properties );
            const filter_unique = function( value, index, self ) {
              return self.indexOf(value) === index;
            };
            referral_models = referral_models.filter( filter_unique );
            for( let i = 0; i < referral_models.length; i++ ) {
              referral_models = getTypesFromModels( instance.userGroupsApiModels[ referral_models[i] ] );
            }
            referral_models = referral_models.filter( filter_unique );
            return referral_models;
          }
        };
        this.exchangeApiApies.forEach(element => {
          element.referral_models = getReferralModelsFromElement( element, this );
          this.exchangeApiApiesParams.push( element.parameters );
        });
        exchangeTypesArray.forEach(element => {
          if ( element.indexOf('private') >= 0 ) {
            this.exchangeApiTypes.push("Private");
          } else {
            this.exchangeApiTypes.push("Public");
          }
        });
      });
    this.apiDocsService.getDocsApi('wallet')
      .subscribe(result => {
        result.apis.forEach(element => {
          // get type of method ( public or private )
          walletTypesArray.push(element.path);
          this.walletApiApiesPaths.push(element.path);
          element.operations.forEach(element => {
            this.walletApiApies.push(element);
          });
        });
        for (let key in result.models) {
          if (result.models.hasOwnProperty(key)) {
            this.walletApiModels[key] = result.models[key].properties;
          }
        }
        let getReferralModelsFromElement = function( element, instance ) {
          let referral_models = [];
          if( typeof element.type !== 'undefined' ) {
            let tmp_properties = instance.walletApiModels[element.type];
            referral_models.push( element.type );
            const getTypesFromModels = function( real_properties ) {
              for( let key in real_properties ) {
                if(real_properties.hasOwnProperty(key) ) {
                  if( typeof real_properties[key].$ref !== 'undefined' ) {
                    referral_models.push(real_properties[key].$ref);
                  } else if( typeof real_properties[key].items !== 'undefined' && typeof real_properties[key].items.$ref !== 'undefined' ) {
                    referral_models.push(real_properties[key].items.$ref);
                  }
                }
              }
              return referral_models;
            };
            referral_models = getTypesFromModels( tmp_properties );
            const filter_unique = function( value, index, self ) {
              return self.indexOf(value) === index;
            };
            referral_models = referral_models.filter( filter_unique );
            for( let i = 0; i < referral_models.length; i++ ) {
              referral_models = getTypesFromModels( instance.userGroupsApiModels[ referral_models[i] ] );
            }
            referral_models = referral_models.filter( filter_unique );
            return referral_models;
          }
        };
        this.walletApiApies.forEach(element => {
          element.referral_models = getReferralModelsFromElement( element, this );
          this.walletApiApiesParams.push( element.parameters );
        });
        walletTypesArray.forEach(element => {
          if ( element.indexOf('private') >= 0 ) {
            this.walletApiTypes.push("Private");
          } else {
            this.walletApiTypes.push("Public");
          }
        });
      });
    this.apiDocsService.getDocsApi('pos')
      .subscribe(result => {
        result.apis.forEach(element => {
          // get type of method ( public or private )
          posTypesArray.push(element.path);
          this.posApiApiesPaths.push(element.path);
          element.operations.forEach(element => {
            this.posApiApies.push(element);
          });
        });
        let getReferralModelsFromElement = function( element, instance ) {
          let referral_models = [];
          if( typeof element.type !== 'undefined' ) {
            let tmp_properties = instance.posApiModels[element.type];
            referral_models.push( element.type );
            const getTypesFromModels = function( real_properties ) {
              for( let key in real_properties ) {
                if(real_properties.hasOwnProperty(key) ) {
                  if( typeof real_properties[key].$ref !== 'undefined' ) {
                    referral_models.push(real_properties[key].$ref);
                  } else if( typeof real_properties[key].items !== 'undefined' && typeof real_properties[key].items.$ref !== 'undefined' ) {
                    referral_models.push(real_properties[key].items.$ref);
                  }
                }
              }
              return referral_models;
            };
            referral_models = getTypesFromModels( tmp_properties );
            const filter_unique = function( value, index, self ) {
              return self.indexOf(value) === index;
            };
            referral_models = referral_models.filter( filter_unique );
            for( let i = 0; i < referral_models.length; i++ ) {
              referral_models = getTypesFromModels( instance.userGroupsApiModels[ referral_models[i] ] );
            }
            referral_models = referral_models.filter( filter_unique );
            return referral_models;
          }
        };
        this.posApiApies.forEach(element => {
          element.referral_models = getReferralModelsFromElement( element, this );
          this.posApiApiesParams.push( element.parameters );
        });
        for (let key in result.models) {
          if (result.models.hasOwnProperty(key)) {
            this.posApiModels[key] = result.models[key].properties;
          }
        }
        posTypesArray.forEach(element => {
          if ( element.indexOf('private') >= 0 ) {
            this.posApiTypes.push("Private");
          } else {
            this.posApiTypes.push("Public");
          }
        });
      });
  }

  formSubmit(path, method, value, index, jsonFile) {
    if ( method === 'POST' ) {
      Object.keys(value).forEach(k => (!value[k] && value[k] !== undefined) && delete value[k]);
      // console.log(value);
      this.apiDocsService.parametersCheck(path, value)
        .subscribe(
            result => {
          // console.log('result', result);
          // console.log('get headers', result.headers);
          // console.log(jsonFile);
          if ( jsonFile.slice(4) === 'common.json' ) {
            this.commonRequestUrl[index] = 'http://showcase.draglet.com/gateway' + path;
            if ( JSON.stringify(result.body) ) {
              this.commonResponseBody[index] = JSON.stringify(result.body);
            } else {
              this.commonResponseBody[index] = 'no content';
            }
            this.commonResponseCode[index] = result.status;
            this.commonResponseHeaders[index] = result.headers;
            this.commonRequestResult[index] = true;
          } else if ( jsonFile.slice(4) === 'user-groups.json' ) {
            this.userGroupsRequestUrl[index] = 'http://showcase.draglet.com/gateway' + path;
            if ( JSON.stringify(result.body) ) {
              this.userGroupsResponseBody[index] = JSON.stringify(result.body);
            } else {
              this.userGroupsResponseBody[index] = 'no content';
            }
            this.userGroupsResponseCode[index] = result.status;
            this.userGroupsResponseHeaders[index] = result.headers;
            this.userGroupsRequestResult[index] = true;
          } else if ( jsonFile.slice(4) === 'exchange.json' ) {
            this.exchangeRequestUrl[index] = 'http://showcase.draglet.com/gateway' + path;
            if ( JSON.stringify(result.body) ) {
              this.exchangeResponseBody[index] = JSON.stringify(result.body);
            } else {
              this.exchangeResponseBody[index] = 'no content';
            }
            this.exchangeResponseCode[index] = result.status;
            this.exchangeResponseHeaders[index] = result.headers;
            this.exchangeRequestResult[index] = true;
          } else if ( jsonFile.slice(4) === 'wallet.json' ) {
            this.walletRequestUrl[index] = 'http://showcase.draglet.com/gateway' + path;
            if ( JSON.stringify(result.body) ) {
              this.walletResponseBody[index] = JSON.stringify(result.body);
            } else {
              this.walletResponseBody[index] = 'no content';
            }
            this.walletResponseCode[index] = result.status;
            this.walletResponseHeaders[index] = result.headers;
            this.walletRequestResult[index] = true;
          } else if ( jsonFile.slice(4) === 'pos.json' ) {
            this.posRequestUrl[index] = 'http://showcase.draglet.com/gateway' + path;
            if ( JSON.stringify(result.body) ) {
              this.posResponseBody[index] = JSON.stringify(result.body);
            } else {
              this.posResponseBody[index] = 'no content';
            }
            this.posResponseCode[index] = result.status;
            this.posResponseHeaders[index] = result.headers;
            this.posRequestResult[index] = true;
          }
        },
          error => {
            if ( jsonFile.slice(4) === 'common.json' ) {
              this.commonRequestUrl[index] = 'http://showcase.draglet.com/gateway' + path;
              if ( JSON.stringify(error.body) ) {
                this.commonResponseBody[index] = JSON.stringify(error.body);
              } else {
                this.commonResponseBody[index] = 'no content';
              }
              this.commonResponseCode[index] = error.status;
              this.commonResponseHeaders[index] = error.headers;
              this.commonRequestResult[index] = true;
            } else if ( jsonFile.slice(4) === 'user-groups.json' ) {
              this.userGroupsRequestUrl[index] = 'http://showcase.draglet.com/gateway' + path;
              if ( JSON.stringify(error.body) ) {
                this.userGroupsResponseBody[index] = JSON.stringify(error.body);
              } else {
                this.userGroupsResponseBody[index] = 'no content';
              }
              this.userGroupsResponseCode[index] = error.status;
              this.userGroupsResponseHeaders[index] = error.headers;
              this.userGroupsRequestResult[index] = true;
            } else if ( jsonFile.slice(4) === 'exchange.json' ) {
              this.exchangeRequestUrl[index] = 'http://showcase.draglet.com/gateway' + path;
              if ( JSON.stringify(error.body) ) {
                this.exchangeResponseBody[index] = JSON.stringify(error.body);
              } else {
                this.exchangeResponseBody[index] = 'no content';
              }
              this.exchangeResponseCode[index] = error.status;
              this.exchangeResponseHeaders[index] = error.headers;
              this.exchangeRequestResult[index] = true;
            } else if ( jsonFile.slice(4) === 'wallet.json' ) {
              this.walletRequestUrl[index] = 'http://showcase.draglet.com/gateway' + path;
              if ( JSON.stringify(error.body) ) {
                this.walletResponseBody[index] = JSON.stringify(error.body);
              } else {
                this.walletResponseBody[index] = 'no content';
              }
              this.walletResponseCode[index] = error.status;
              this.walletResponseHeaders[index] = error.headers;
              this.walletRequestResult[index] = true;
            } else if ( jsonFile.slice(4) === 'pos.json' ) {
              this.posRequestUrl[index] = 'http://showcase.draglet.com/gateway' + path;
              if ( JSON.stringify(error.body) ) {
                this.posResponseBody[index] = JSON.stringify(error.body);
              } else {
                this.posResponseBody[index] = 'no content';
              }
              this.posResponseCode[index] = error.status;
              this.posResponseHeaders[index] = error.headers;
              this.posRequestResult[index] = true;
            }
        }
          );
    } else {
      this.apiDocsService.getParameters(path)
        .subscribe(
            result => {
          // console.log('result', result);
          // console.log(jsonFile);
          if ( jsonFile.slice(4) === 'common.json' ) {
            this.commonRequestUrl[index] = 'http://showcase.draglet.com/gateway' + path;
            if ( JSON.stringify(result.body) ) {
              this.commonResponseBody[index] = JSON.stringify(result.body);
            } else {
              this.commonResponseBody[index] = 'no content';
            }
            this.commonResponseCode[index] = result.status;
            this.commonResponseHeaders[index] = result.headers;
            this.commonRequestResult[index] = true;
          } else if ( jsonFile.slice(4) === 'user-groups.json' ) {
            this.userGroupsRequestUrl[index] = 'http://showcase.draglet.com/gateway' + path;
            if ( JSON.stringify(result.body) ) {
              this.userGroupsResponseBody[index] = JSON.stringify(result.body);
            } else {
              this.userGroupsResponseBody[index] = 'no content';
            }
            this.userGroupsResponseCode[index] = result.status;
            this.userGroupsResponseHeaders[index] = result.headers;
            this.userGroupsRequestResult[index] = true;
          } else if ( jsonFile.slice(4) === 'exchange.json' ) {
            this.exchangeRequestUrl[index] = 'http://showcase.draglet.com/gateway' + path;
            if ( JSON.stringify(result.body) ) {
              this.exchangeResponseBody[index] = JSON.stringify(result.body);
            } else {
              this.exchangeResponseBody[index] = 'no content';
            }
            this.exchangeResponseCode[index] = result.status;
            this.exchangeResponseHeaders[index] = result.headers;
            this.exchangeRequestResult[index] = true;
          } else if ( jsonFile.slice(4) === 'wallet.json' ) {
            this.walletRequestUrl[index] = 'http://showcase.draglet.com/gateway' + path;
            if ( JSON.stringify(result.body) ) {
              this.walletResponseBody[index] = JSON.stringify(result.body);
            } else {
              this.walletResponseBody[index] = 'no content';
            }
            this.walletResponseCode[index] = result.status;
            this.walletResponseHeaders[index] = result.headers;
            this.walletRequestResult[index] = true;
          } else if ( jsonFile.slice(4) === 'pos.json' ) {
            this.posRequestUrl[index] = 'http://showcase.draglet.com/gateway' + path;
            if ( JSON.stringify(result.body) ) {
              this.posResponseBody[index] = JSON.stringify(result.body);
            } else {
              this.posResponseBody[index] = 'no content';
            }
            this.posResponseCode[index] = result.status;
            this.posResponseHeaders[index] = result.headers;
            this.posRequestResult[index] = true;
          }
        },
          error => {
            if ( jsonFile.slice(4) === 'common.json' ) {
              this.commonRequestUrl[index] = 'http://showcase.draglet.com/gateway' + path;
              if ( JSON.stringify(error.body) ) {
                this.commonResponseBody[index] = JSON.stringify(error.body);
              } else {
                this.commonResponseBody[index] = 'no content';
              }
              this.commonResponseCode[index] = error.status;
              this.commonResponseHeaders[index] = error.headers;
              this.commonRequestResult[index] = true;
            } else if ( jsonFile.slice(4) === 'user-groups.json' ) {
              this.userGroupsRequestUrl[index] = 'http://showcase.draglet.com/gateway' + path;
              if ( JSON.stringify(error.body) ) {
                this.userGroupsResponseBody[index] = JSON.stringify(error.body);
              } else {
                this.userGroupsResponseBody[index] = 'no content';
              }
              this.userGroupsResponseCode[index] = error.status;
              this.userGroupsResponseHeaders[index] = error.headers;
              this.userGroupsRequestResult[index] = true;
            } else if ( jsonFile.slice(4) === 'exchange.json' ) {
              this.exchangeRequestUrl[index] = 'http://showcase.draglet.com/gateway' + path;
              if ( JSON.stringify(error.body) ) {
                this.exchangeResponseBody[index] = JSON.stringify(error.body);
              } else {
                this.exchangeResponseBody[index] = 'no content';
              }
              this.exchangeResponseCode[index] = error.status;
              this.exchangeResponseHeaders[index] = error.headers;
              this.exchangeRequestResult[index] = true;
            } else if ( jsonFile.slice(4) === 'wallet.json' ) {
              this.walletRequestUrl[index] = 'http://showcase.draglet.com/gateway' + path;
              if ( JSON.stringify(error.body) ) {
                this.walletResponseBody[index] = JSON.stringify(error.body);
              } else {
                this.walletResponseBody[index] = 'no content';
              }
              this.walletResponseCode[index] = error.status;
              this.walletResponseHeaders[index] = error.headers;
              this.walletRequestResult[index] = true;
            } else if ( jsonFile.slice(4) === 'pos.json' ) {
              this.posRequestUrl[index] = 'http://showcase.draglet.com/gateway' + path;
              if ( JSON.stringify(error.body) ) {
                this.posResponseBody[index] = JSON.stringify(error.body);
              } else {
                this.posResponseBody[index] = 'no content';
              }
              this.posResponseCode[index] = error.status;
              this.posResponseHeaders[index] = error.headers;
              this.posRequestResult[index] = true;
            }
          });
    }
  }

  getApiDocs() {
    this.getApiDocsList();
    this.getApiDocsPage();
  }


  ngAfterContentInit() {
    this.getApiDocs();
  }

}
