import { Component, OnInit } from '@angular/core';
import { ModalService } from '../../modal.service';

@Component({
  selector: 'app-exchange',
  templateUrl: './exchange.component.html',
  styleUrls: ['./exchange.component.scss'],
})
export class ExchangeComponent implements OnInit {

  constructor(private modalService: ModalService) {
  }

  openModal() {
    this.modalService.openModal();
  }

  ngOnInit() {
  }

}
