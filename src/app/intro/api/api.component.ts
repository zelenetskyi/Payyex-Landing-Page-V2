import { Component, OnInit } from '@angular/core';
import { ModalService } from '../../modal.service';

@Component({
  selector: 'app-api',
  templateUrl: './api.component.html',
  styleUrls: ['./api.component.scss'],
})
export class ApiComponent implements OnInit {

  constructor(private modalService: ModalService) {
  }

  openModal() {
    this.modalService.openModal();
  }

  ngOnInit() {
  }

}
