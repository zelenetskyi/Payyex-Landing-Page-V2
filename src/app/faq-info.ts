export var faqList = [
  {
    "id": 0,
    "question": "What is Bitcoin?",
    "answer": "Please have a look at the „what is Bitcoin“ page which will give you a small introduction on the topic of “Bitcoin”.",
    "likes": 14,
    "active": true
  },
  {
    "id": 1,
    "question": "How can I join the payyex exchange?",
    "answer": "<ul><li>Just register on http://payyex.com and confirm the e-mail verification.</li><li>Log In.</li><li>There you go – open your first orders.</li></ul>",
    "likes": 14,
    "active": true
  },
  {
    "id": 2,
    "question": "Is it possible to place orders that are not covered by my account balance?",
    "answer": "Yes! We decided to enable the placing of orders that actually are not covered by available balance. Those orders have the status “paused” and will be transformed to “open”, executable orders as soon as balance is available. This enables our users to place a chain of currently not executable orders that will be matched one after another when the first order could be successfully matched and gave the necessary balance.",
    "likes": 14,
    "active": true
  },
  {
    "id": 3,
    "question": "What are \"contradicting orders\"?",
    "answer": "It is possible to place orders without having the needed balance to execute them. As a result, there exist combinations of orders that will result in a shrinking balance. For example: <div><ul><li>You place a limited order, sell 100 BTC 650€, but you currently have no Bitcoins. The order is placed and has the status \"paused\".</li><li>Afterwards, You place an unlimited order, buy 100 BTC.</li><li>As soon as the unlimited order get executed, you would instantly sell your Bitcoins for 650€, which is 50€ per BTC lower than you have procured them. Each Bitcoin makes you lose 50€ (transaction fees also have to be applied).</li></ul></div>It is not possible to place this constellation of orders, because those would result in a sure loss. You will get an error message which tells you, which order is contradicting with the order you were about to place.",
    "likes": 14,
    "active": true
  },
  {
    "id": 4,
    "question": "Why don't I get credited the full value of my executed Order?",
    "answer": "For every executed order you pay 0,5% transaction fees. This applies for buy and sell orders alike. If you sell 10 Bitcoin at 500 EUR/BTC, the price is 5,000.00 Euro plus fees. Thus 0.5% virtual transaction fee is subtracted and you get credited 4,975.00€. Currently there is no way to lower your transaction fees.",
    "likes": 14,
    "active": true
  },
  {
    "id": 5,
    "question": "How can I create a new order?",
    "answer": "Either go to the „Orders“ or „Account“ site. From either page you may place a new order directly. You can specify the type of the order, the amount of traded Bitcoins and limit the price. In general, you can place following orders:<ul><li>Buy order (I want to buy Bitcoin) - limited and unlimited</li><li>Sell order (I want to sell Bitcoin) - limited and unlimited</li></ul>",
    "likes": 14,
    "active": true
  },
  {
    "id": 6,
    "question": "How can I cancel orders?",
    "answer": "Go to the „Orders“ page and check at your current orders. There is a clickable button marked with an X on the left side of every open or paused order. After clicking you can cancel the order by confirming the subsequent dialog.",
    "likes": 14,
    "active": true
  },
  {
    "id": 7,
    "question": "Is it possible to cancel executed orders?",
    "answer": "payyex is an exchange software which automatically matches and executes orders. Being able to cancel an executed order means to conflict with the safe execution of trades. We enable safe trades that cannot be reverted, giving our users full security. Giving the opportunity to cancel or revert executed trades endangers the safety of trades.",
    "likes": 14,
    "active": true
  },
  {
    "id": 8,
    "question": "What are „best execution” orders?",
    "answer": "Execute best orders (or „at best” orders) will be matched with the best available price on the market, therefore getting matched instantly (if possible). „At best” execution orders only specify the amount of Bitcoin which are supposed to be sold/bought and, in contrary to limited orders, cannot be specified with price limit. “Best execution” orders are best if:<ul><li>You have no knowledge of the current market price structure.</li><li>You want to buy or sell a certain amount of Bitcoins as quickly as possible.</li></ul>",
    "likes": 14,
    "active": true
  },
  {
    "id": 9,
    "question": "What are „limited“ orders?",
    "answer": "Limited orders are specified with the amount of Bitcoins that should be sold/bought and the price limit that you want to pay/earn at most at least per Bitcoin. The order will only be executed if a matching order within the specified price range is available. If no matching order exists, the order cannot be executed and waits for execution. „Limited orders” are great for:<ul><li>Holding Bitcoins for a longer period of time and sell them at a higher exchange rate in the future. For example: You bought 1 Bitcoin for 500 Euro and you want to sell it, when the exchange rate hits 700€ / BTC. You place the order: Limited Sell-order, Amount: 1 BTC, Limit: 700€.</li><li>Trying to buy Bitcoins with a cheap exchange rate. For example, you plan to buy 10 Bitcoins when the exchange rate is at 400 €/BTC. You place the Limited Buy-order: Amount: 10 BTC, Limit: 400€.</li></ul>",
    "likes": 14,
    "active": true
  },
  {
    "id": 10,
    "question": "Is it possible to get a bad price by placing a limited order?",
    "answer": "The order matching is designed to match orders according to the current market price.<ul><li>If you place a sell order and choose a limit of 1 €/BTC, your sell order will be matched with the current market price, but at least 1 €/BTC. If the highest bid is 510 €/BTC and the highest ask is 500 €/BTC, you will get 510 €/BTC when selling your Bitcoins. If the highest current bid is at 490 €/BTC and the lowest current ask is 500 €/BTC, you will get 490€ for your sell order. Transaction fees are not considered in this example.</li><li>If you want to buy a Bitcoin and place your buy order with 1000 €/BTC as limit, the order will still be matched with the current market price, at a maximum of 1000€ per Bitcoin. If the currently highest bid is at 490 €/BTC and the market price (ask) is at 500 €/BTC, you will be able to buy Bitcoins for 500€. If the current, lowest Ask is 510 €/BTC and the market price is at 500 €/BTC, you will be matched for 510 €/BTC. Transaction fees are not considered in this example.</li></ul>",
    "likes": 14,
    "active": true
  },
  {
    "id": 11,
    "question": "How can I restore my password?",
    "answer": "Please click on the link: „Forgot password”. In the subsequent step, you will have to fill in the e-mail address you registered with on our site. After triggering the recovery e-mail, you can click the link in the e-mail and will be redirected to a page where you can change your password.",
    "likes": 14,
    "active": true
  },
  {
    "id": 12,
    "question": "How can I change my nickname?",
    "answer": "You can change your nickname by opening your profile page and then simply typing in your new nickname. If the nickname already was assigned to another participant, you will receive a notification. After choosing your new nickname, please click on “save profile” – your nickname has been changed and will be updated in the ranking.",
    "likes": 14,
    "active": true
  },
  {
    "id": 13,
    "question": "How can I change my e-mail address?",
    "answer": "Your e-mail address can be changed in your profile. Simply open the profile page and change your e-mail address in the corresponding field. After changing your e-mail address and saving the profile you will receive a confirmation e-mail which is sent to your new e-mail address. In order to complete the change of the e-mail address, you will have to click the confirmation link in the e-mail. After this you are able to log into your account with your new e-mail address.",
    "likes": 14,
    "active": true
  },
  {
    "id": 14,
    "question": "What can I do when I encounter a problem that is not discussed here?",
    "answer": "Please contact either our support (support@payyex.com) or use one of the feedback boxes to contact us.",
    "likes": 14,
    "active": true
  },
  {
    "id": 15,
    "question": "How do I get informed about the current development of the Bitcoin exchange game?",
    "answer": "You will receive an e-mail every 14 days that informs you about the current development of the Bitcoin exchange game.",
    "likes": 14,
    "active": true
  },
  {
    "id": 16,
    "question": "When will the second round start and end?",
    "answer": "The second round of the payyex Bitcoin bootcamp starts 9th of March 2014 and ends 9th of June 2014, 12:00 AM (CET).",
    "likes": 14,
    "active": true
  },
  {
    "id": 17,
    "question": "What happens after the end of the round?",
    "answer": "After the end of the round, every participant in the top 100 will get his share of the prize distribution and all scores will be set to the starting amount, the trading will be momentarily stopped. If you want to continue playing you can do so. Just wait for the new round to begin and commence playing.",
    "likes": 14,
    "active": true
  },
  {
    "id": 18,
    "question": "When will new trading tools be implemented?",
    "answer": "Trading tools will be implemented in the upcoming rounds.",
    "likes": 14,
    "active": true
  },
  {
    "id": 19,
    "question": "What are all the feedback boxes for?",
    "answer": "With the feedback boxes you have the opportunity to give us feedback on almost everything. If you have a problem while using our website, if you want another way of displaying your account status or if you wish to place new types of orders – just tell us!",
    "likes": 14,
    "active": true
  },
  {
    "id": 20,
    "question": "What is the „payyex wallet“?",
    "answer": "The payyex wallet lets payyex users access the prizes from the Bitcoin bootcamp and transfer them. It is also possible to receive Bitcoin and store them in a safe environment.",
    "likes": 14,
    "active": true
  },
  {
    "id": 21,
    "question": "Do I have to pay anything for using the payyex wallet?",
    "answer": "No, all wallet services are free.",
    "likes": 14,
    "active": true
  },
  {
    "id": 22,
    "question": "What is the transaction fee of the payyex Bitcoin wallet?",
    "answer": "The transaction fee is paid directly to the Bitcoin network, to support the work of the network. It is 0.0001 BTC and is necessary if transactions < 0.01 BTC are processed and if Bitcoin are reused within a short period. As payyex uses a hot wallet, we cannot be sure that Bitcoin remain untouched long enough to allow a reuse without transaction fee. This is why we require a transaction fee for every transaction.",
    "likes": 14,
    "active": true
  },
  {
    "id": 23,
    "question": "How can I get a payyex wallet account?",
    "answer": "Simply sign up for the payyex Bitcoin bootcamp. Every participant automatically gets access to the wallet.",
    "likes": 14,
    "active": true
  },
  {
    "id": 24,
    "question": "What is a two-factor-authentication (2FA)?",
    "answer": "Two-factor-authentication is based on using additional security features besides the login (username and password). payyex offers two alternatives for the 2FA, sending e-mail tokens and using Google Authenticator. Both alternatives can also be combined for maximum security. You are able to choose different security settings for particular actions.",
    "likes": 14,
    "active": true
  },
  {
    "id": 25,
    "question": "How does e-mail authentication work?",
    "answer": "E-mail authentication can be configured in the profile settings. After the activation you receive codes by e-mail when you use a function where you have activated it.",
    "likes": 14,
    "active": true
  },
  {
    "id": 26,
    "question": "What is Google Authenticator and how does the setup work?",
    "answer": "Google Authenticator is an app for smarthpones which can be downloaded for free in the stores/markets of the different smartphone manufactureres. After installing the authenticator app you have to set up a new account. You press “Add Google Authenticator” in the profile of the payyex wallet, and you receive a personal key that you enter (or scan the QR code) in order to link your payyex account with the Google Authenticator. After you linked your account you get a new OTP (one time password) every 30 seconds which can be used for example to log in, if you decided to protect your log in with GA. You can find a detailed manual here: <a href='https://support.google.com/accounts/answer/1066447?hl=eng'>https://support.google.com/accounts/answer/1066447?hl=eng</a>",
    "likes": 14,
    "active": true
  },
  {
    "id": 27,
    "question": "What can I do when I added a Google Authenticator that I cannot access anymore?",
    "answer": "Please contact our support at (support@payyex.com) .",
    "likes": 14,
    "active": true
  }
];
